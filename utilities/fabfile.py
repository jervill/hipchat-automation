__author__ = 'pratikshah'
from fabric.api import *
from fabric.api import run, local
from utilities.keys import VAGRANT_PATH, VAGRANT_HOST
from utilities.constants import INSTALL_SCRIPT_PATH
import os

env.hosts = VAGRANT_HOST
env.user = "vagrant"
env.password = "vagrant"
env.warn_only = True
env.keepalive = 1

def get_number_of_sessions():
    run("redis-cli KEYS \"sessions:*\" ")


def purge_sessions():
    with hide('running', 'stdout'):
        status = vagrant_status()
        if status:
            print "Clear sessions on " + env.hosts
            run("redis-cli KEYS \"sessions:*\" | xargs redis-cli DEL")
        else:
            print "Sorry, VM is down, try helper -vm up"

def restart_all():
    status = vagrant_status()
    if status:
        print "Restart services on " + env.hosts
        run("sudo service allthethings restart")
    else:
        print "Sorry, VM is down, try helper -vm up"


def vagrant_status():
    with hide('running', 'stdout'):
        print "Checking if vagrant is up."
        result = local(command="cd " + VAGRANT_PATH + " ; vagrant status", capture=True)
        if 'running' in result and 'hipchat' in result:
            print "VM is up."
            return True
        else:
            print "VM is down."
            return False


def vagrant_up():
    status = vagrant_status()
    if status:
        print 'VM is running, use -vm reload.'
    else:
        print "Starting your VM."
        local(command="cd " + VAGRANT_PATH + " ; vagrant up")
        run("sudo service allthethings restart")


def vagrant_reload():
    status = vagrant_status()
    if status:
        print "VM will halt and reload."
        local(command="cd " + VAGRANT_PATH + " ; vagrant reload")
    else:
        print "No need to reload, doing vagrant up."
        local(command="cd " + VAGRANT_PATH + " ; vagrant up")

def vagrant_provision():
    status = vagrant_status()
    if status:
        print "VM will reload with provisioning."
        local(command="cd " + VAGRANT_PATH + " ; vagrant reload --provision")
    else:
        print "No need to reload, doing vagrant up."
        local(command="cd " + VAGRANT_PATH + " ; vagrant up")

def vagrant_halt():
    with hide('running', 'stdout'):
        status = vagrant_status()
        if status:
            print "Halting VM."
            local(command="cd " + VAGRANT_PATH + " ; vagrant halt", capture=True)
        else:
            print "VM is not running, no need to halt."


# calls install bash script

def install():
    with hide():
        local(command=INSTALL_SCRIPT_PATH)


def install_update():
    with hide():
        local(command=( INSTALL_SCRIPT_PATH + ' update'))

# remote script execution on HC VM
def install_logio():
    with hide():
        print "running on dev VM.."
        put(INSTALL_SCRIPT_PATH, mode=0755)
        sudo('/mnt/hipchat/hipchat-automation/./install.sh logio')


def logio_restart():
    with hide():
        sudo(command="ps axww | grep log.io-server | awk '{ print $1 }' | xargs kill")
        sudo(command="ps axww | grep log.io-harvester | awk '{ print $1 }' | xargs kill -9")
        run(command="sudo /mnt/hipchat/hipchat-automation/./logio.sh")



def reset_hipchat_data():
    with hide():
        # reset all
        run("cd $HIPCHAT_SRC/ops && rake reset_with_fire")
        run("redis-cli flushall")



def help():
    local(command="helper -h")