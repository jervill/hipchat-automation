import datetime
import random
from faker import Faker



def hc_data(num_of_users):
    users = []
    while num_of_users != 0:
        num = str(random_number())
        fake = Faker()
        fname = fake.first_name()
        lname = fake.last_name().replace('\'',"")
        fullname = fname + " " + lname
        email = (fname + '.' + lname + num + '@atlassian.com').lower()
        room = fullname + " Test Room"
        mention = fname + lname + num
        title = "Atlassian Employee"
        users.append({"fname": fname, "lname": lname, "email": email, "name": fullname, "mention": mention, "room": room, "title": title })
        num_of_users -= 1
    return users

def get_key_from_url(url):
    key = url.rsplit('/', 1)
    return key[1]


def return_time():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d_%H:%M:%S")

def random_number():
    return random.randint(0,99)

def generate_public_room_name():
    return "Public Room " + str(random_number())


def generate_private_room_name():
    return "Private Room " + str(random_number())


def generate_rename_room():
    curr_time = return_time()
    return "Automation Rename Room " + curr_time




def generate_topic_name():
    curr_time = return_time()
    return "Automation Topic " + curr_time


def generate_room_message():
    curr_time = return_time()
    return "Automation Room Message " + curr_time


def generate_email_address():
    email = 'automation_%s@testing.net' % str(random_number())
    return email

def generate_user_name():
    return 'automation user' + str(random_number())
