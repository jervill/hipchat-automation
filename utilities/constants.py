__author__ = 'pratikshah'
import os

PYTHONPATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/'
CRAPPIE_ENV_PATH = PYTHONPATH + 'configs/environments/hosts.cfg'
DATA_CONFIG_PATH = PYTHONPATH + 'configs/data/data_config.cfg'
FAB_PATH = PYTHONPATH + 'utilities/'
FAB_FILE = PYTHONPATH + 'utilities/fabfile.py'
CRAPPIE_TEST_PATH = 'tests/crappie'
INSTALL_SCRIPT_PATH = PYTHONPATH + './install.sh'
