__author__ = 'pratikshah'

import ConfigParser
import os

from packages.sst.config import flags
from utilities.constants import CRAPPIE_ENV_PATH


config = ConfigParser.RawConfigParser()
env_config_file = os.path.join(CRAPPIE_ENV_PATH)
config.read(env_config_file)

VAGRANT_PATH = config.get('vagrant', 'vagrant_path')
VAGRANT_HOST = config.get('vagrant', 'host')

class Keys():

    def __init__(self):
        pass

    @staticmethod
    def get_environment():
        selected_environment = flags[0]

        # Test for the existence of the selected environment, then gather its items into a dictionary.
        if config.has_section(selected_environment):
            env = dict(config.items(selected_environment))
            return env
        else:
            print "The environment %s could not be found. Please doublecheck the the environment config file '%s'" % (selected_environment, env_config_file)
            exit()
