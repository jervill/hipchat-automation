__author__ = 'pratikshah'
import sqlite3
import sys
import os
import ConfigParser
import MySQLdb
import itertools
from packages.hypchat import *
from fabric.api import settings, run
from utilities.constants import DATA_CONFIG_PATH

config = ConfigParser.ConfigParser()
config.read(DATA_CONFIG_PATH)




def get_simpsons_data():
    db = sqlite3.connect('simpsons.db')
    print "connection successful."
    db.row_factory = dict_factory
    cur = db.cursor()
    cur.execute("SELECT * FROM data_users")
    rows = cur.fetchall()
    return rows


def get_hipchat_data():
    db = MySQLdb.connect(host="192.168.33.10", user="root", passwd="yesiwishthat", db="hipchat")
    cursor = db.cursor()
    cursor.execute(""" SELECT * FROM users """)
    my_dict = dictfetchall(cursor)
    return my_dict


def get_hipchat_user_id(email):
    db = MySQLdb.connect(host="192.168.33.10", user="root", passwd="yesiwishthat", db="hipchat")
    cursor = db.cursor()
    cursor.execute(""" SELECT id FROM users where email = '%s';""" % email)
    my_dict = dictfetchall(cursor)
    return my_dict[0]['id']


def get_all_hipchat_ids():
    db = MySQLdb.connect(host="192.168.33.10", user="root", passwd="yesiwishthat", db="hipchat")
    cursor = db.cursor()
    cursor.execute(""" SELECT email FROM users""")
    my_dict = dictfetchall(cursor)
    return my_dict


def dictfetchall(cursor):
    """Returns all rows from a cursor as a list of dicts"""
    desc = cursor.description
    return [dict(itertools.izip([col[0] for col in desc], row))
            for row in cursor.fetchall()]


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def user_exists(email):
    db = MySQLdb.connect(host="192.168.33.10", user="root", passwd="yesiwishthat", db="hipchat")
    cursor = db.cursor()
    cursor.execute(""" SELECT * FROM users where email = '%s' """ % email)
    my_dict = dictfetchall(cursor)
    return bool(my_dict)


def room_exists(room):
    db = MySQLdb.connect(host="192.168.33.10", user="root", passwd="yesiwishthat", db="hipchat")
    cursor = db.cursor()
    cursor.execute(""" SELECT * FROM rooms where name = '%s' """ % room)
    my_dict = dictfetchall(cursor)
    return bool(my_dict)


def seed():
    confirm = raw_input('Your about to reset your data back to a known state, please confirm [Y/n]: ')
    if confirm.lower().strip() == 'y':
        # reset hipchat to prestine state
        reset_db()

        # get user input of token and URL

        AUTH_TOKEN = raw_input('Generate token for alice@example.com paste here: ')
        AUTH_TOKEN = AUTH_TOKEN.strip()
        ENDPOINT = 'http://devvm.hipchat.com'
        hipchat = HypChat(AUTH_TOKEN, ENDPOINT)

        # get all rows of seed data
        rows = get_simpsons_data()

        # create users with image
        for row in rows:

            admin = bool(row['is_group_admin'])
            email = row['email']
            firstname = row['firstname']
            fullname = row['fullname']
            title = row['title']
            mentionname = row['mentionname']

            print ("Creating user %s with email %s" % (fullname, email))
            result = user_exists(email)
            if result is False:
                hipchat.create_user(name=fullname, email=email, title=title,
                                    mention_name=mentionname, is_group_admin=admin,
                                    password='password')
                image_path = str(os.path.abspath('.')) + '/images/' + firstname + '.jpg'
                if os.path.isfile(image_path):
                    print 'uploading %s image' % firstname
                    hipchat.update_photo(email, image_path=image_path)
            else:
                print  "User exists, no need to create."

        # create a room for each user
        for row in rows:
            email = row['email']
            fullname = row['fullname']
            room_name = row['room_names']

            user_id = get_hipchat_user_id(email)
            print ("Creating room named %s owned by %s" % (room_name, fullname))
            result = room_exists(room=room_name)
            if result is False:
                hipchat.create_room(name=room_name, owner=user_id)
            else:
                print "Room exists, no need to create."

        # send 1-1 message
        for row in rows:
            fullname = row['fullname']
            message = row['messages']

            print '%s is sending messages...' % fullname
            all_user_ids = get_all_hipchat_ids()
            for item in all_user_ids:
                hipchat.send_private_message_user(id_or_email=item['email'], message=message, message_format='text')
        # Restart all
        restart_allthings()


def reset_db():
        with settings(host_string='192.168.33.10', user='vagrant', password="vagrant"):
            run("cd /mnt/hipchat/ops && rake reset_with_fire")
            run("redis-cli flushall")
            run("sudo service allthethings restart")


def restart_allthings():
        with settings(host_string='192.168.33.10', user='vagrant', password="vagrant"):
         run("sudo service allthethings restart")


def add_users():
    pass

def main():
    seed()


if __name__ == '__main__':
    sys.exit(main())

