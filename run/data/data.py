import sys
import argparse
import ConfigParser
import redis
import json
from packages.hypchat import *
from utilities.constants import DATA_CONFIG_PATH
from utilities.utils import hc_data

myredis = redis.StrictRedis(host='192.168.33.10',port=6379, db=0)

def get_keys(key_pattern):
    return myredis.keys(key_pattern)

def increase_rate_limit():
    keys = get_keys("OauthClient*")

    for key in keys:
        curr_key = json.loads(myredis.get(key))
        curr_key['ratelimit'] = 50000
        updated_key = json.dumps(curr_key)
        myredis.set(name=key, value=updated_key)

config = ConfigParser.ConfigParser()
config.read(DATA_CONFIG_PATH)

if config.has_section('HipChat'):
    AUTH_TOKEN = config.get('HipChat', 'token')
    ENDPOINT = config.get('HipChat', 'endpoint')
else:
    exit("please see HipChat section of your config file in %s", DATA_CONFIG_PATH)

hipchat = HypChat(AUTH_TOKEN, ENDPOINT)


def create_hipchat_users(num_of_users, admin=False):
    increase_rate_limit()
    user_data = hc_data(num_of_users=num_of_users)
    for data in user_data:
        email = data['email']
        name = data["name"]
        mention = data['mention']
        title = "Doctor Feel Good"
        print "Creating hc_data %s with email %s, mention name of @%s, a group admin status of %s" % (
            name, email, mention, admin)
        hipchat.create_user(email=email, name=name, mention_name=mention, is_group_admin=admin, password='password',
                            title=title)


def create_hipchat_rooms(num_rooms, admin=False):
    increase_rate_limit()
    rooms = hc_data(num_rooms)
    for room_data in rooms:
        room = room_data['room']
        print "Creating public room %s" % room
        # TODO: need to  add privacy type and guest access
        hipchat.create_room(room, owner='')

def data_generator(args):
    if 'api.hipchat.com' in ENDPOINT:
        exit("cannot create users on production, change endpoint in data_config file.")
    if args.users:
        if (args.users <= 5000) and (args.users > 0):
            create_hipchat_users(num_of_users=args.users)
        else:
            exit("Max 5000 users.")

    if args.rooms:
        if (args.rooms <= 5000) and (args.rooms > 0):
            create_hipchat_rooms(num_rooms=args.rooms)
        else:
            exit("Max 5000 rooms.")

    if args.rooms and args.users:
        if (args.rooms <= 5000) and (args.rooms > 0):
            pass

def arguments():
    parser = argparse.ArgumentParser(
        description='generate data, example: data -u 100, data -p 100')
    parser.add_argument('-u', '--users', required=False, action='store', type=int,
                        help='enter number of users you want to create.')
    parser.add_argument('-r', '--rooms', required=False, action='store', type=int,
                        help='enter number of rooms you want to create.')
    return parser.parse_args()


def main():
    return data_generator(arguments())


if __name__ == '__main__':
    sys.exit(main())

    import sys
import argparse
import ConfigParser
import redis
import json
from packages.hypchat import *
from utilities.constants import DATA_CONFIG_PATH
from utilities.utils import hc_data

myredis = redis.StrictRedis(host='192.168.33.10',port=6379, db=0)

def get_keys(key_pattern):
    return myredis.keys(key_pattern)

def increase_rate_limit():
    keys = get_keys("OauthClient*")

    for key in keys:
        curr_key = json.loads(myredis.get(key))
        curr_key['ratelimit'] = 50000
        updated_key = json.dumps(curr_key)
        myredis.set(name=key, value=updated_key)

config = ConfigParser.ConfigParser()
config.read(DATA_CONFIG_PATH)

if config.has_section('HipChat'):
    AUTH_TOKEN = config.get('HipChat', 'token')
    ENDPOINT = config.get('HipChat', 'endpoint')
else:
    exit("please see HipChat section of your config file in %s", DATA_CONFIG_PATH)

hipchat = HypChat(AUTH_TOKEN, ENDPOINT)


def create_hipchat_users(num_of_users, admin=False):
    increase_rate_limit()
    user_data = hc_data(num_of_users=num_of_users)
    for data in user_data:
        email = data['email']
        name = data["name"]
        mention = data['mention']
        title = "Doctor Feel Good"
        print "Creating hc_data %s with email %s, mention name of @%s, a group admin status of %s" % (
            name, email, mention, admin)
        hipchat.create_user(email=email, name=name, mention_name=mention, is_group_admin=admin, password='password',
                            title=title)


def create_hipchat_rooms(num_rooms, admin=False):
    increase_rate_limit()
    rooms = hc_data(num_rooms)
    for room_data in rooms:
        room = room_data['room']
        print "Creating public room %s" % room
        # TODO: need to  add privacy type and guest access
        hipchat.create_room(room, owner='')

def data_generator(args):
    if 'api.hipchat.com' in ENDPOINT:
        exit("cannot create users on production, change endpoint in data_config file.")
    if args.users:
        if (args.users <= 5000) and (args.users > 0):
            create_hipchat_users(num_of_users=args.users)
        else:
            exit("Max 5000 users.")

    if args.rooms:
        if (args.rooms <= 5000) and (args.rooms > 0):
            create_hipchat_rooms(num_rooms=args.rooms)
        else:
            exit("Max 5000 rooms.")

    if args.rooms and args.users:
        if (args.rooms <= 5000) and (args.rooms > 0):
            pass

def arguments():
    parser = argparse.ArgumentParser(
        description='generate data, example: data -u 100, data -p 100')
    parser.add_argument('-u', '--users', required=False, action='store', type=int,
                        help='enter number of users you want to create.')
    parser.add_argument('-r', '--rooms', required=False, action='store', type=int,
                        help='enter number of rooms you want to create.')
    return parser.parse_args()


def main():
    return data_generator(arguments())


if __name__ == '__main__':
    sys.exit(main())

