__author__ = 'pratikshah'
import redis
import json


myredis = redis.StrictRedis(host='192.168.33.10',port=6379, db=0)

def get_keys(key_pattern):
    return myredis.keys(key_pattern)

keys = get_keys("OauthClient*")

for key in keys:
    curr_key = json.loads(myredis.get(key))
    curr_key['ratelimit'] = 50000
    updated_key = json.dumps(curr_key)
    print updated_key
    myredis.set(name=key, value=updated_key)

    #myredis.hincrby(name=key, key="ratelimit", amount=30)
    #print myredis.get(key)
    print myredis.get(key)

#    myredis.set(name=key, value='4000', ex=600)
