__author__ = 'pratikshah'
import argparse
import sys
import os
from utilities.constants import FAB_FILE


def run(args):

    # hc system
    if args.restart_services:
        fab_cmd("restart_all")
    if args.purge_sessions:
        fab_cmd("purge_sessions")

    # vm

    if args.vm == 'up':
        fab_cmd("vagrant_up")
    if args.vm == 'status':
        fab_cmd("vagrant_status")
    if args.vm == 'reload':
        fab_cmd("vagrant_reload")
    if args.vm == 'halt':
        fab_cmd("vagrant_halt")
    if args.vm == 'provision':
        fab_cmd('vagrant_provision')
    if args.vm == 'logio':
        fab_cmd('logio_restart')

    # install script

    if args.install and args.install == "install":
        fab_cmd("install")
    if args.install and args.install == "logio":
        fab_cmd("install_logio")
    if args.install and args.install == "update":
        fab_cmd("install_update")

    if args.logio and args.logio == "restart":
        fab_cmd("logio_restart")

    fab_cmd("help")

def fab_cmd(fab_func):
    os.system("fab -f " + FAB_FILE + " " + fab_func)
    exit()


def arguments():
    if ('-vm' not in sys.argv and '-i' not in sys.argv and 'logio' not in sys.argv) and len(sys.argv) > 2:
        print "error: only one flag is allowed."
        exit(fab_cmd("helper"))

    parser = argparse.ArgumentParser(description="hipchat environment tools: VM, Services, etc.")
    parser.add_argument('-restart', '--restart_services', action='store_const', const='restart', required=False,
                        help="example: helper -r ")
    parser.add_argument('-purge', '--purge_sessions', action='store_const', const='purge', required=False,
                        help="example: helper -p ")
    parser.add_argument('-i', '--install', action='store', required=False,
                        help="example:  helper -i [ install | update | logio  ] ")
    parser.add_argument('-vm', '--vm', action='store', required=False,
                        help="example: helper -vm [ up | halt | reload | status | provision ] ")
    parser.add_argument('-logio', '--logio', action='store', required=False,
                        help="example: helper -logio restart ")
    return parser.parse_args()


def main():
    return run(arguments())


if __name__ == "__main__":
    sys.exit(main())