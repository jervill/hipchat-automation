__author__ = 'pratikshah'
import sys
import os
import argparse
from utilities.constants import PYTHONPATH, CRAPPIE_TEST_PATH


def tests_runner(args):
    os.chdir(PYTHONPATH)

    if args.debug:
        os.system("sst-run " + args.test + " " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=localhost " + args.debug )
    elif args.environment:
        os.system("sst-run " + args.test + " " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=" + args.environment)
    elif args.environment and args.debug:
        os.system( "sst-run " + args.test + " " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=" + args.environment + " " + args.debug)
    else:
        os.system("sst-run " + args.test + " " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=localhost " )


def arguments():
    parser = argparse.ArgumentParser(description='run a single test by name.  example: crappie_single -t room_create')
    parser.add_argument('-t', '--test', required=True, action='store', help='Please enter a hipchat test.  It will be ran using Chrome.')
    parser.add_argument('-db', '--debug', required=False, action='store_const', const='--debug', help='puts test run in debug mode.')
    parser.add_argument('-e', '--environment', required=False, action='store', help='Run against specific environment.')
    return parser.parse_args()



def main():
    return tests_runner(arguments())

if __name__ == '__main__':
    sys.exit(main())
