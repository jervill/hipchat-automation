__author__ = 'pratikshah'
import sys
import os
import argparse
from utilities.constants import PYTHONPATH, CRAPPIE_TEST_PATH


def tests_runner(args):
    os.chdir(PYTHONPATH)

    if args.environment:
        print args.environment
        os.system("sst-run -d " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=%s" % args.environment)
    else:
        os.system("sst-run -d " + CRAPPIE_TEST_PATH + " -r xml -b Chrome --with-flags=localhost")

def arguments():
    parser = argparse.ArgumentParser(description='Run all smoke tests against localhost by default.  -e lets you run against a specific environment.')
    parser.add_argument('-e', '--environment', required=False, action='store', help='provide a  hipchat environment.')
    return parser.parse_args()


def main():
    """
    Hook to use with setup.py to create a console script
    """
    return tests_runner(arguments())

if __name__ == '__main__':
    sys.exit(main())