#!/usr/bin/env bash


start_server()
{

echo -e 'Start process logio server\n'
set +e
results=$(nohup log.io-server > /dev/null 2>&1 &)
set -e
echo $results
}

start_harvester()
{

echo -e "Start process logio harvester.\n"
set +e
results=$(nohup log.io-harvester > /dev/null 2>&1 &)
set -e
echo $results
}



setup()
{
start_server
start_harvester 
}


setup

