# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='run.scripts',
    version='1.0',
    description="Hipchat Python scripts for smoke and regression tests. ",
    long_description=""" Run tests using scripts for smoke, regression, etc. """,
    packages=find_packages('run'),
    package_dir={'': 'run'},
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'single=run.scripts.single:main',
            'smoke=run.scripts.smoke:main',
            'helper=run.scripts.helper:main',
            'data=run.data.data:main',
            'seed=run.data.data_seed:main'
        ]
    },
    install_requires=[]
)
