__author__ = 'pratikshah'
from packages.sst.actions import *

class PresencePage(object):

    def __init__(self):
        pass

    @staticmethod
    def click_presence_dropdown_link():
        jquery_click_element(""" $("a[class='aui-dropdown2-trigger hc-header-user-avatar']")[0] """)

    @staticmethod
    def click_available_status():
        jquery_click_element(""" $("a[data-show='chat']")[0]  """)

    @staticmethod
    def click_away_status():
        jquery_click_element(""" $("a[data-show='xa']")[0]  """)

    @staticmethod
    def click_dnd_status():
        jquery_click_element(""" $("a[data-show='dnd']")[0] """)

    @staticmethod
    def write_presence_message(message):
        jquery_write_value("""$('input[name="status-message"]')""", message)

    @staticmethod
    def click_ok():
        jquery_click_element(""" $("button:contains(OK)")  """)
        sleep(2)

    @staticmethod
    def get_presence_icon_type():
        #TODO: remove sleep.
        sleep(2)
        presence_class = jquery_get_attribute_value(""" $("span[data-reactid='.0.0.1.0.1.0.2.0.1']").children().first().attr("class") """)
        if 'icon-dnd' in presence_class:
            return 'dnd'
        if 'icon-away' in presence_class:
            return 'away'
        if 'icon-chat' in presence_class:
            return 'available'