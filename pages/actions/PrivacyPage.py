__author__ = 'pratikshah'
from packages.sst.actions import *


class PrivacyPage(object):

    def __init__(self):
        pass


    @staticmethod
    def click_private_radio_button():
        jquery_click_element(""" $('button:contains(Cancel)')[0] """)
        sleep(5)

    @staticmethod
    def get_room_privacy_status():
        class_attr_value = execute_script(""" return $('div[class="page-header-icon"]').children().attr("class") """)
        if 'public' in class_attr_value:
            return 'public'
        if 'private' in class_attr_value:
            return 'private'

    @staticmethod
    def click_open_radio_button():
        jquery_click_element(""" $("#public-room")[0] """)

    @staticmethod
    def click_private_radio_button():
        jquery_click_element(""" $("#private-room")[0] """)
        sleep(5)

    @staticmethod
    def click_set_privacy_button():
        jquery_click_element(""" $('button:contains(Set privacy)')[0] """)
        sleep(5)

    @staticmethod
    def click_cancel_button():
        jquery_click_element(""" $('button:contains(Cancel)')[0] """)