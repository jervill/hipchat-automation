__author__ = 'pratikshah'
from packages.sst.actions import *

class ArchivePage(object):

    def __init__(self):
        pass

    @staticmethod
    def click_confirm_archive_button():
        jquery_click_element(""" $('button:contains(Archive)') """)

    @staticmethod
    def click_confirm_unarchive_button():
        jquery_click_element(""" $('button:contains(Unarchive)')[0] """)

    @staticmethod
    def click_cancel_archive_button():
        jquery_click_element(""" $('button:contains(Cancel)')[0] """)