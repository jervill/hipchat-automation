__author__ = 'pratikshah'
from packages.sst.actions import *


class InviteYourTeamPage(object):

    def __init__(self):
        pass


    @staticmethod
    def fill_email_address(email):
        result = execute_script("$('.loaded').contents().find('#email_input').val('%s')" % email)
        return result

    @staticmethod
    def click_add_button():
        #Iframe.in
        jquery_click_element(""" $('.loaded').contents().find('#email_add_button')[0]  """)
        sleep(3)

    @staticmethod
    def click_send_invites_button():
        #Iframe.
        jquery_click_element(""" $('.loaded').contents().find('#btn_send_invites')[0] """ )

    @staticmethod
    def enable_email_add_buttion():
        # Iframe
        jquery_click_element("$('.loaded').contents().find('#email_add_button').attr('aria-disabled','')")

    @staticmethod
    def click_send_more_invites():
        jquery_click_element("$('.loaded').contents().find('$('a:contains('Send some more invites')')')")

    @staticmethod
    def get_all_emails_in_table():
        # get all rows
        return jquery_get_text_values(" $('.loaded').contents().find('#emails_to_add').children().children().children().first() ")



