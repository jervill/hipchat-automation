__author__ = 'pratikshah'
from packages.sst.actions import *


class InvitesSentPage(object):

    def __init__(self):
        pass

    @staticmethod
    def get_invite_confirmation_message():
        sleep(4)
        return jquery_get_text_values(" $('.loaded').contents().find('#email_sent_details') ")