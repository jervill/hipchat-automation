__author__ = 'pratikshah'
from packages.sst.actions import *

class LobbyPage(object):

    def __init__(self):
        pass


    @staticmethod
    def click_create_room_button():
        jquery_click_element(""" $('#create-room-button') """)


    @staticmethod
    def click_room_filter_button():
        jquery_click_element(""" $('button:contains(Rooms)') """)

    @staticmethod
    def click_lobby_icon_link():
        jquery_click_element(""" $('span:contains(Lobby)') """)

    @staticmethod
    def click_a_room():
        # TODO: remove sleep.
        sleep(3)
        jquery_click_element(""" $('span:contains(Lobby)')[0] """)
        jquery_click_element(""" $('button:contains(Rooms)')[0] """)
        # TODO: remove sleep.
        sleep(1)
        jquery_click_element(""" $('tbody').children('tr').children('td').children().last()[0] """)


    @staticmethod
    def get_all_room_names():
        # TODO: ensure lobby page is visible.
        jquery_click_element(""" $('a[class="aui-nav-item aui-nav-selected"]')[0] """)
        refresh()
        return jquery_get_text_values(""" $('td[class="hc-lobby-list-name"]').children('a') """)

    @staticmethod
    def get_lobby_room_names():
        return jquery_get_text_values(""" $('ul[class="aui-nav hc-sidebar-nav hc-sortable hc-rooms"]') """)