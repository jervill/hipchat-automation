__author__ = 'pratikshah'
from packages.sst.actions import *
from utilities.keys import Keys as GetKeys

keys = GetKeys.get_environment()


class RoomPage(object):

    """

    """

    def __init__(self):
        # each OnDemand setup will have a room with the same name as the sub-domain.
        self.room = keys['environment']
        self.chat_url = 'https://%s.hipchat.com/chat/?v=2?focus_jid=2_%s@conf.%s.hipchat.com' % (self.room, self.room, self.room)

    @property
    def messaging_textarea(self):
        element = get_element(id="hc-message-input")
        return element


    @staticmethod
    def write_chat_message(message):
        sleep(3)
        message = " document.getElementById('hc-message-input').value = '%s' " % message
        print(message)
        execute_script(message)

    @staticmethod
    def submit_chat_message():
        #TODO: remove sleep
        sleep(3)
        execute_script("""
            var event = document.createEvent("Events");
            event.keyCode = 13
            event.which = 13
            event.initEvent("keydown", true, true);
            document.getElementById("hc-message-input").dispatchEvent(event); """)

    @staticmethod
    def verify_last_message(message):
        last_message_text = execute_script("return $('.scroll-wrap').children('.date-block').children().last().children().last().children().last().text()")
        wait_for(assert_equal, message, last_message_text)

    @staticmethod
    def verify_at_mention(message):
        last_message_text = execute_script("return $('.scroll-wrap').children('.date-block').children().last().children().last().children().last().text()")
        wait_for(assert_equal, message, last_message_text)

