__author__ = 'pratikshah'
from packages.sst.actions import *


class CreateRoomPage(object):

    def __init__(self):
        pass

    @staticmethod
    def verify_page():
        if not exists_element(id='create-room-name'):
            exit("Sorry, page verification page for %s failed. Id %s was not found.") % ('Create Room Page', 'create-room-name')

    @staticmethod
    def fill_new_room_fields(room_name,topic_name=''):
        jquery_write_value("$('#create-room-name')", room_name)
        jquery_write_value("$('#create-room-topic')", topic_name)

    @staticmethod
    def click_open_radio_button():
        jquery_click_element(""" $('#public-room') """)

    @staticmethod
    def click_private_radio_button():
        jquery_click_element(""" $('#private-room') """)


    @staticmethod
    def click_confirm_create_room_button():
        jquery_click_element(""" $('button:contains(Create room)') """)
        sleep(5)

    @staticmethod
    def click_create_room_in_lobby():
        jquery_click_element(""" $('span:contains(Create a room)')[0] """)

    @property
    def exists_blanket(self):
        return exists_element(css_class="aui-blanket")
