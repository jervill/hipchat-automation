__author__ = 'pratikshah'
from packages.sst.actions import *


class RenamePage(object):

    def __init__(self):
        pass

    @property
    def room_name_input(self):
        element = get_element(id='room-name')
        return element

    @staticmethod
    def click_rename_button():
        jquery_click_element(""" $("button:contains('Rename')")[0] """)
        # TODO: replace sleep here.
        sleep(3)