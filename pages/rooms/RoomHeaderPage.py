__author__ = 'pratikshah'
from packages.sst.actions import *


class RoomHeaderPage(object):

    def __init__(self):
        pass

    @staticmethod
    def click_room_actions_button():
        jquery_click_element(""" $('#room-actions-btn') """)
        sleep(3)

    @staticmethod
    def get_room_privacy_status():
        return jquery_get_attribute_value(""" $('.page-header-icon').children('span').attr('class') """)


    @staticmethod
    def get_room_name():
        return jquery_get_text_values(""" $('h3') """)