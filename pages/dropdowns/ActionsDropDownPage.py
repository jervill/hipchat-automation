__author__ = 'pratikshah'
from packages.sst.actions import *


class ActionsDropDownPage(object):

    def __init__(self):
        pass



    @staticmethod
    def click_room_actions_rename():
        jquery_click_element(""" $('a[title="Rename"]')[0] """)

    @staticmethod
    def click_room_actions_change_topic():
        jquery_click_element(""" $('a:contains(Change Topic)')[0] """)

    @staticmethod
    def click_room_actions_archive():
        jquery_click_element(""" $('a:contains(Archive)')[0] """)

    @staticmethod
    def click_room_actions_unarchive():
        jquery_click_element(""" $('a:contains(Unarchive)')[0] """)

    @staticmethod
    def click_room_actions_delete():
        jquery_click_element(""" $('a:contains(Delete)')[0] """)

    @staticmethod
    def click_room_actions_privacy():
        jquery_click_element(""" $('a:contains(Change Privacy)')[0] """)