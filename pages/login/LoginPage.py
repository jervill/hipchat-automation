__author__ = 'pratikshah'
import os

from packages.sst.actions import *
from pages.lobby.LobbyPage import LobbyPage
from utilities.keys import Keys as GetKeys


keys = GetKeys.get_environment()
LobbyPage = LobbyPage()

user = keys['username']
pwd = keys['password']

class LoginPage(object):

    def __init__(self):
        self.environment = keys['environment']
        print self.environment
        self.url = 'https://%s.hipchat.com/chat/?v=2' % self.environment

    @staticmethod
    def write_username():
        jquery_write_value(" $('#email') ", user)

    @staticmethod
    def write_password():
        jquery_write_value(" $('#password')  ", pwd)


    @staticmethod
    def click_login_button():
        jquery_click_element(""" $('#signin') """)



    def login_hipchat(self, url=None):
        if self.environment == 'localhost':
            os.system('crappie_env -p')

        if url:
            go_to(url)
        else:
            go_to(self.url)
        self.write_username()
        self.write_password()
        self.click_login_button()
        sleep(3)
        LobbyPage.click_lobby_icon_link()
        wait_for(exists_element, id='create-room-button')
        #TODO: remove sleep
        sleep(3)




    def login(self, url=None):
        """
        Note: calling test must define assertion for page landing after login is complete.
        """

        os.system('crappie_env -p')
        if url:
            go_to(url)
        else:
            go_to(self.url)
        self.write_username()
        self.write_password()
        self.click_login_button()
