__author__ = 'pratikshah'
from packages.sst.actions import jquery_click_element

class ChatGlobalPage(object):

    @staticmethod
    def click_invite_team_link():
        jquery_click_element(""" $('a:contains(Invite your team)')[0] """)

    @staticmethod
    def click_lobby_invite_team_link():
        jquery_click_element(""" $('span:contains(Invite your team)')[0] """)