from packages.sst.actions import *
from pages.login.LoginPage import LoginPage
from pages.hipchat_global.HipChatGlobalPage import ChatGlobalPage
from pages.rooms.CreateRoomPage import CreateRoomPage
from pages.lobby.LobbyPage import LobbyPage


LoginPage = LoginPage()
ChatHomePage = ChatGlobalPage()
CreateRoomPage = CreateRoomPage()
LobbyPage = LobbyPage()

LoginPage.login_hipchat()
LobbyPage.click_create_room_button()
CreateRoomPage.click_confirm_create_room_button()

#assert validation chat
exists_element(tag='div', text="Every room needs a name.")