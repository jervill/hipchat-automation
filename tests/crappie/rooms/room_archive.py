__author__ = 'pratikshah'
from packages.sst.actions import *
from pages.login.LoginPage import LoginPage
from pages.lobby.LobbyPage import LobbyPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.rooms.RenamePage import RenamePage
from pages.dropdowns.ActionsDropDownPage import ActionsDropDownPage
from pages.actions.ArchivePage import ArchivePage

# Page definition

LoginPage = LoginPage()
LobbyPage = LobbyPage()
RenamePage = RenamePage()
RoomHeaderPage = RoomHeaderPage()
ActionsDropdownPage = ActionsDropDownPage()
ArchivePage = ArchivePage()


# test
LoginPage.login_hipchat()
LobbyPage.click_a_room()

# data generation
room_name = RoomHeaderPage.get_room_name()

RoomHeaderPage.click_room_actions_button()
ActionsDropDownPage.click_room_actions_archive()
ArchivePage.click_confirm_archive_button()
sleep(5)

# assert here


RoomHeaderPage.click_room_actions_button()
ActionsDropdownPage.click_room_actions_unarchive()
ArchivePage.click_confirm_unarchive_button()
sleep(5)

# assert here
