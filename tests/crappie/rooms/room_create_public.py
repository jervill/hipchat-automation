from nose import tools

from utilities.utils import generate_public_room_name
from pages.login.LoginPage import LoginPage
from pages.hipchat_global.HipChatGlobalPage import ChatGlobalPage
from pages.rooms.CreateRoomPage import CreateRoomPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.lobby.LobbyPage import LobbyPage


LoginPage = LoginPage()
ChatHomePage = ChatGlobalPage()
CreateRoomPage = CreateRoomPage()
LobbyPage = LobbyPage()
RoomHeaderPage = RoomHeaderPage()

# data
public_room = generate_public_room_name()

# Test
LoginPage.login_hipchat()
LobbyPage.click_create_room_button()

CreateRoomPage.fill_new_room_fields(public_room)
CreateRoomPage.click_open_radio_button()
CreateRoomPage.click_confirm_create_room_button()


# Assertion
tools.assert_in('public', RoomHeaderPage.get_room_privacy_status())

