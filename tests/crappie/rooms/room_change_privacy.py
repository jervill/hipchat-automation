__author__ = 'pratikshah'
from nose import tools
from pages.login.LoginPage import LoginPage
from pages.lobby.LobbyPage import LobbyPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.rooms.RenamePage import RenamePage
from pages.dropdowns.ActionsDropDownPage import ActionsDropDownPage
from pages.actions.PrivacyPage import PrivacyPage

# Page definition
LoginPage = LoginPage()
LobbyPage = LobbyPage()
RenamePage = RenamePage()
RoomHeaderPage = RoomHeaderPage()
ActionsDropdownPage = ActionsDropDownPage()
PrivacyPage = PrivacyPage()


# test
LoginPage.login_hipchat()
LobbyPage.click_a_room()

original_privacy_status = PrivacyPage.get_room_privacy_status()

RoomHeaderPage.click_room_actions_button()
ActionsDropdownPage.click_room_actions_privacy()

# check room type and click opposite
if original_privacy_status == 'private':
    PrivacyPage.click_open_radio_button()
elif original_privacy_status == 'public':
    PrivacyPage.click_private_radio_button()

PrivacyPage.click_set_privacy_button()
new_privacy_status = PrivacyPage.get_room_privacy_status()


# assertion: verify old and new privacy status is not equal.
tools.assert_not_equal(original_privacy_status, new_privacy_status)

