from nose import tools

from utilities.utils import generate_public_room_name
from pages.login.LoginPage import LoginPage
from pages.hipchat_global.HipChatGlobalPage import ChatGlobalPage
from pages.rooms.CreateRoomPage import CreateRoomPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.lobby.LobbyPage import LobbyPage


LoginPage = LoginPage()
ChatHomePage = ChatGlobalPage()
CreateRoomPage = CreateRoomPage()
LobbyPage = LobbyPage()
RoomHeaderPage = RoomHeaderPage()

public_room = generate_public_room_name()

LoginPage.login_hipchat()
CreateRoomPage.click_create_room_in_lobby()

CreateRoomPage.fill_new_room_fields(public_room)
CreateRoomPage.click_private_radio_button()
CreateRoomPage.click_confirm_create_room_button()

CreateRoomPage.click_confirm_create_room_button()

tools.assert_in(public_room, LobbyPage.get_lobby_room_names())

