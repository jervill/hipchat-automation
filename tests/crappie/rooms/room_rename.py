from nose import tools

from packages.sst.actions import *
from utilities.utils import generate_rename_room
from pages.login.LoginPage import LoginPage
from pages.lobby.LobbyPage import LobbyPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.rooms.RenamePage import RenamePage
from pages.dropdowns.ActionsDropDownPage import ActionsDropDownPage


# Page definition

LoginPage = LoginPage()
LobbyPage = LobbyPage()
RenamePage = RenamePage()
RoomHeaderPage = RoomHeaderPage()
ActionsDropdownPage = ActionsDropDownPage()



# test
LoginPage.login_hipchat()
LobbyPage.click_a_room()

# data generation
existing_room_name = RoomHeaderPage.get_room_name()
new_room_name = generate_rename_room()

RoomHeaderPage.click_room_actions_button()
ActionsDropDownPage.click_room_actions_rename()
sleep(3)
write_textfield(RenamePage.room_name_input, new_room_name)

# TODO: replace by waits or a write method in Jquery
RenamePage.click_rename_button()

changed_room_name = RoomHeaderPage.get_room_name()
tools.assert_equal(new_room_name, changed_room_name)
