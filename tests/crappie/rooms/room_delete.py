__author__ = 'pratikshah'
import os

from nose import tools

from pages.login.LoginPage import LoginPage
from pages.lobby.LobbyPage import LobbyPage
from pages.rooms.RoomPage import RoomPage
from pages.rooms.RoomHeaderPage import RoomHeaderPage
from pages.rooms.RenamePage import RenamePage
from pages.dropdowns.ActionsDropDownPage import ActionsDropDownPage
from pages.actions.DeletePage import DeletePage



# Page Definition

LoginPage = LoginPage()
LobbyPage = LobbyPage()
RenamePage = RenamePage()
RoomHeaderPage = RoomHeaderPage()
RoomPage = RoomPage()
ActionsDropDownPage = ActionsDropDownPage()
DeletePage = DeletePage()


# Test

LoginPage.login_hipchat()
LobbyPage.click_a_room()

room_name = RoomHeaderPage.get_room_name()
print room_name
room_name = ""

if room_name is None or room_name is "":
    os.system('data_gen -r 1')
    room_name = RoomHeaderPage.get_room_name()
RoomHeaderPage.click_room_actions_button()
ActionsDropDownPage.click_room_actions_delete()
DeletePage.click_delete_button()

# Assertion: room name is not found in the list of room names.
LobbyPage.click_lobby_icon_link()
all_rooms = LobbyPage.get_all_room_names()

tools.assert_not_in(room_name, all_rooms, "Sorry %s was not removed." % room_name )

