from packages.sst.actions import *

from pages.login.LoginPage import LoginPage
from pages.presence.PresencePage import PresencePage

# pages
LoginPage = LoginPage()
PresencePage = PresencePage()

# test

LoginPage.login_hipchat()
PresencePage.click_presence_dropdown_link()
PresencePage.click_away_status()
PresencePage.write_presence_message('I am away!')
PresencePage.click_ok()
status = PresencePage.get_presence_icon_type()
assert_equal('away', status)

PresencePage.click_presence_dropdown_link()
PresencePage.click_dnd_status()
PresencePage.write_presence_message('I am dnd!')
PresencePage.click_ok()
status = PresencePage.get_presence_icon_type()
assert_equal('dnd', status)


PresencePage.click_presence_dropdown_link()
PresencePage.click_available_status()
PresencePage.write_presence_message('I am available!')
PresencePage.click_ok()
status = PresencePage.get_presence_icon_type()
assert_equal('available', status)