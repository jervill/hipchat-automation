from packages.sst.actions import *

from pages.login.LoginPage import LoginPage
from pages.lobby.LobbyPage import LobbyPage
from pages.rooms.RoomPage import RoomPage
from utilities.utils import return_time as curr_time

# pages
LoginPage = LoginPage()
LobbyPage = LobbyPage()
RoomPage = RoomPage()

# data: must leave trailing space.
message = '@here ' + curr_time()

# test
LoginPage.login_hipchat()
LobbyPage.click_a_room()
wait_for(assert_element, id='hc-message-input')
RoomPage.write_chat_message(message)
RoomPage.submit_chat_message()

# assertion
RoomPage.verify_at_mention(message.strip('@'))

