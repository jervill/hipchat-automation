__author__ = 'pratikshah'
from nose import tools
from utilities.utils import *
from pages.login.LoginPage import LoginPage
from pages.rooms.RoomPage import RoomPage
from pages.hipchat_global.HipChatGlobalPage import ChatGlobalPage
from pages.invite.InviteYourTeamPage import InviteYourTeamPage
from pages.invite.InvitesSentPage import InvitesSentPage


# Pages
LoginPage = LoginPage()
ChatPage = RoomPage()
ChatGlobalPage = ChatGlobalPage()
InviteYourTeamPage = InviteYourTeamPage()
InvitesSentPage = InvitesSentPage()

# Data
email = generate_email_address()

# TestCase
LoginPage.login_hipchat()
ChatGlobalPage.click_invite_team_link()
InviteYourTeamPage.fill_email_address(email)

InviteYourTeamPage.enable_email_add_buttion()
InviteYourTeamPage.click_add_button()

# Assertion : assert that email entered is listed in the table below
emails_in_table = InviteYourTeamPage.get_all_emails_in_table()
tools.assert_equals(email, emails_in_table, "Sorry, email wasn't added to invite user table list.")

InviteYourTeamPage.click_send_invites_button()
# Assertion : assert that invitation sent and the Invites Sent page is displayed
confirmation_message = InvitesSentPage.get_invite_confirmation_message()
tools.assert_in('1 email is on its way. Happy chatting!', confirmation_message, "Sorry, couldn't find confirmation message.")
