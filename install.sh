#!/bin/bash


# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"

CHROMEDRIVER='/usr/bin/chromedriver'
PHANTOMDRIVER='/usr/bin/phantomjs'


PROJECT_DIRECTORY=$PWD
VENV_DIRECTORY=$HOME/.virtualenvs
AUTOMATION_VENV_DIR=$HOME/.virtualenvs/automation
AUTOMATION_VENV_BIN_DIR=${AUTOMATION_VENV_DIR}/bin


COUNT=$#

install_logio()
{
     install_dependencies
     add_repos
     install_nodejs
     logio
     copy_harvester_conf
     exit 0

}


setup()
{

      deactivate_venv
      install_packages
      venv_creation
      venv_requirements
      copy_drivers
      set_pythonpath
      instructions
      echo -e "$COL_RESET"
}

update()
{
      update_environment
      echo -e "$COL_RESET"

}

deactivate_venv()
{


 if [ ${VIRTUAL_ENV} ] ; then
    echo 'Please deactivate virtualenv. Type in deactivate.'
    exit 0
 fi

}


install_packages()
{
#install global packages
packages="pip"

        echo -e "$COL_CYAN"
        echo -e " Begin install of global packages.."
        get_environment=$(uname -srv)

        if [[ ${get_environment} = *"Darwin"* ]]; then
            for package in ${packages}
            do
                set +e
                    echo -e "\ninstalling global package '${package}' \n"
                    echo -e $(sudo easy_install ${package} )
                set -e
            done

        elif [[ ${get_environment} = *"Ubuntu"* ]]; then
             for package in ${packages}
             do
                        set +e
                        echo -e "\ninstalling '${package}\n' "
                        echo $(sudo apt-get install ${package})
                        set -e
             done
         else
            echo "Sorry, didn't recognize what environment you're on, you must be on OSX or Ubuntu."
             exit 0
         fi
        echo -e "\nFinished install of global packages\n"


# install global pip packages
pip_packages="virtualenv virtualenvwrapper"
        echo -e "Begin install of pip packages.\n"
         for pip_package in ${pip_packages}
            do
                    echo -e "\ninstalling pip package ${pip_package}\n"
                    set +e
                    results=$(sudo pip install ${pip_package})
                    set -e
                    echo ${results}
            done
        echo -e "Finished install of pip packages.\n"



brew_packages='redis'

        echo -e "Begin install of brew packages.\n"
         for brew_package in ${brew_packages}
            do
                        echo -e "\ninstalling brew package ${brew_package}\n"
                        set +e
                        results=$(brew install ${brew_package})
                        set -e
                        echo ${results}
            done
        echo -e "Finished install of brew packages.\n"
}




venv_creation()
{

#create deletion and creation automation venv.

    echo -e "$COL_BLUE"

    if [ -d "$AUTOMATION_VENV_DIR" ]; then
        echo "There was a existing 'automation' virtualenv, deleting it."
        set +e
        results=$(sudo rm -rf ${AUTOMATION_VENV_DIR})
        set -e
        echo ${results}
        echo -e "deleted venv.\n"
    fi

    echo -e 'Creating a fresh automation virtualenv.\n'
    set +e
    source /usr/local/bin/virtualenvwrapper.sh
    mkvirtualenv automation
    set -e
    echo $results
    echo -e "Completed 'automation' virtualenv creation.\n"


}


venv_requirements()
{

      echo -e "$COL_MAGENTA"
      echo -e "installing venv pip requirements\n"

      set +e
      source ${AUTOMATION_VENV_DIR}/bin/activate
      results=$(pip install -r requirements.txt)
      set -e
      echo -e "${results}\n"
      echo -e "Successfully installed pip requirements.\n"


}


copy_drivers()
{

    echo -e "$COL_GREEN"

  echo -e "Copying webdriver browser files.\n"

    if [ -f "${CHROMEDRIVER}" ] && [ -f "${PHANTOMDRIVER}" ]
        then
        echo -e "Webdriver files exist, no need to copy.\n"
    else
        set +e
        results=$(sudo cp configs/drivers/* /usr/bin/)
        set -e
        echo -e "${results}\n"
        echo -e "Successfully copied webdriver browser files.\n"
  fi

}


set_pythonpath()
{
    echo -e "$COL_YELLOW"
    echo -e "Setting pythonpath in postactivate file,\n"
    echo -e '#!/bin/bash\nexport PYTHONPATH='${PROJECT_DIRECTORY} > ${AUTOMATION_VENV_DIR}/bin/postactivate
    cat ${AUTOMATION_VENV_DIR}/bin/postactivate
    sudo chmod +x $PROJECT_DIRECTORY/packages/sst/scripts/sst-run
    sudo ln -s $PROJECT_DIRECTORY/packages/sst/scripts/sst-run $AUTOMATION_VENV_BIN_DIR/sst-run
    echo -e "\npythonpath setup completed.\n"

 }

update_environment(){

       if [[ ${VIRTUAL_ENV} == *"automation"* ]] ; then
         echo -e "$COL_YELLOW"
         echo -e "\n---------------------------------------------------------------------------\n"
          sudo rm -rf run/run.*/
          sudo python setup.py develop
         echo -e "\n---------------------------------------------------------------------------\n"

         echo "Completed updating Framework."
          else
          echo "Not on automation venv."
          exit 0

       fi


}

instructions()
{
# remaining steps are manual: activate venv and setup.py develop

      echo -e "$COL_RED"
      echo -e "\n---------------------------------------------------------------------------\n"

      echo -e "COMPLETE REMAINING STEPS:\n"


      echo "1. add lines to .bashrc:"
      echo "source /usr/local/bin/virtualenvwrapper.sh"
      echo "WORKON_HOME=~/.virtualenvs "
      echo -e "alias auto='workon automation' \n"
      echo "2. source ~/.bashrc"
      echo "3. $ workon automation"
      echo "4. $ sudo python setup.py develop"
      echo "5. edit configs/environments/hosts.cfg, fill in localhost section."
      echo "6. edit configs/data/data_config.cfg, fill in HipChat section."
      echo "7. $ smoke"

      echo -e "\n---------------------------------------------------------------------------\n"

}

helper()
{

echo -e "\nCommand Options:\n"
echo -e "./install.sh"
echo -e "./install.sh update"
echo -e "./install.sh help"
echo -e "./install.sh logio"

}










install_dependencies()
{

os=$(uname -v)

  if [[ $os = *"Ubuntu"* ]] || [[ $os = *"ubuntu"* ]]
   then
    echo ""
    else
    echo "Sorry, you must be on an Ubuntu machine. SSH into your VM and run this script."
    exit 0
  fi

#install dependencies
dependencies="python-software-properties g++ make"

        echo "Starting installation of dependencies......."
        set +e
        results=$(sudo apt-get install ${dependencies} -y)
        set -e
        echo $results
        echo "Successfully installed dependencies......."

}


add_repos()
{

        echo "Adding repo and update."
        set +e
        results=$(sudo add-apt-repository ppa:chris-lea/node.js -y)
        results=$(sudo apt-get update)
        set -e
        echo $results
        echo "Sucessfully update repo add and update."


}



install_nodejs()
{

package='nodejs'

      echo "starting installation for ${package}"
      set +e
      results=$(sudo apt-get install nodejs -y)
      set -e
      echo $results
      echo "$Successfully installed nodejs or it exists."



}


logio()
{
package='logio'
user=$(whoami)
      echo "starting installation for ${package}"
        set +e
        results=$(cd $HOME && sudo npm install -g log.io --user $user)
        set -e
      echo $results
      echo "successfully installed ${package}"



}

copy_harvester_conf()
{
  echo "Copying harvester conf file.."

    set +e
    # TODO: this is ghetto.  Change this to a file later.
    results=$(echo  'exports.config =
   {
   nodeName: "dev_logging",
   logStreams:
   {
     nginx_error_log: ["/var/log/nginx/error.log"],
     tetra_log: ["/var/log/hipchat/tetra.log"],
     coral_log: ["/var/log/hipchat/coral.log"],
     web_log:["/var/log/hipchat/web.log"],
     mail_log:["/srv/rsyslog/mail.log"],
     redis_log:["/var/log/hipchat/database.log"] },
     server: {
      host:"0.0.0.0",
      port: 28777
  }
  }'  >> "$HOME/.log.io/harvester.conf"  )
    set -e
    echo $results

      echo "Successfully copied harvester conf file.."
}


help()
{

      echo "******************************************"

      echo -e "To start log server: sudo log.io-server & \n"
      echo -e "To start log server: sudo log.io-harvester \n"
      echo -e "Navigate to http:\\devvm.hipchat.com:28778 \n"
      echo -e "The harvester conf file is located at ~/.log.io/harvester.conf \n"

      echo "******************************************"




}

# project install
if [[ $# -eq 0 ]] ; then
    read -r -p "Are you sure you want to re-install everything? [Y/n] " response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
    then
        setup
    else
        exit 0
    fi
fi

# logio install
if [[ $1 == "logio" ]] ; then

install_logio
exit 0
fi


# project update
if [[ $1 == "update" ]] ; then
update
exit 0
fi

helper