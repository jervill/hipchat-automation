HipChat Automation
====================

### Setup Steps ###

```sh
$ git clone git@bitbucket.org:pratik3/hipchat-automation.git
$ chmod +x .install.sh 
$ ./install.sh
add lines bashrc:
 source /usr/local/bin/virtualenvwrapper.sh
 WORKON_HOME=~/.virtualenvs 
 alias auto='workon automation'
$ source ~/.bashrc
$ workon automation
$ sudo python setup.py develop
edit configs/environments/hosts.cfg, fill in localhost section.
edit configs/data/data_config.cfg, fill in HipChat section.
$ smoke
```
### How to Run Tests ###

note: smoke can also run only those tests that have "smoke" as part of the the name, however I did not add factor this in at this time.

- smoke   (runs a suite of tests defined )
- smoke -e <host defined in hosts.cfg>
- smoke -e  <host> -b [Chrome Firefox PhantomJS ]

- single (runs a single test)
- single -t <testcase file without .py extension > -e <hostname> -b [Chrome Firefox PhantomJS ]

### Tools ###

###### restart hc processes
- helper -r

###### purge sessions
- helper -p

###### vagrant VM
- helper -vm [up | halt | reload | status | provision ]

### Todo's ###
 - Write data generator to seed HC instance